#!/bin/bash

function testinstall {
  if [ $(dpkg-query -W -f='${Status}' $1 2>/dev/null | grep -c "ok installed") -eq 0 ];
  then
    sudo apt-get install $1 -y
  else
    echo -e deb pkg $1'\t'[OK]
  fi
}

testinstall python-smbus
testinstall python-spidev
testinstall python-dev
testinstall python-setuptools

if ! $(python -c "import Adafruit_GPIO" &> /dev/null); then
  testinstall build-essential
  testinstall git
  git clone https://github.com/adafruit/Adafruit_Python_GPIO
  cd Adafruit_Python_GPIO
  sudo python setup.py install
  cd ..
else
  echo -e Python Adafruit_GPIO'\t'[OK]
fi
if ! $(python -c "import Adafruit_PureIO" &> /dev/null); then
  testinstall build-essential
  testinstall git
  git clone https://github.com/adafruit/Adafruit_Python_PureIO
  cd Adafruit_Python_PureIO
  sudo python setup.py install
  cd ..
else
  echo -e Python Adafruit_PureIO'\t'[OK]
fi
if ! $(python -c "import Adafruit_MPR121.MPR121" &> /dev/null); then
  testinstall build-essential
  testinstall git
  git clone https://github.com/adafruit/Adafruit_Python_MPR121
  cd Adafruit_Python_MPR121
  sudo python setup.py install
  cd ..
else
  echo -e Python Adafruit_MPR121'\t'[OK]
fi

sudo cp -v mpr121d.py /usr/local/sbin/.
sudo cp -v etc/mpr121d.conf /etc/.
sudo systemctl disable mpr121d
sudo cp -v etc/systemd/system/mpr121d.service /etc/systemd/system/.
sudo systemctl enable mpr121d
sudo systemctl start mpr121d
sudo systemctl status mpr121d
