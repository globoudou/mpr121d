#!/usr/bin/python

import sys, getopt
import os.path
import ConfigParser
import threading
import Adafruit_MPR121.MPR121 as MPR121
import time

class keyaction(threading.Thread):
  terminated = False
  key = False
  trigger = False
  hold = False
  cmd = False

  def __init__(self, key, trigger, hold, cmd):
    threading.Thread.__init__(self)
    self.terminated = False
    self.key = key
    self.trigger = trigger
    self.hold = hold
    self.cmd = cmd

  def run(self):
    self.on = True
    started = time.time()
    while not self.terminated:
      if time.time() - started > self.hold:
        os.system(self.cmd)
        self.stop()
      time.sleep(0.1)

  def stop(self):
    self.terminated = True

class keyboardActions(object):
  confFile = ''
  conf = []
  def __init__(self):
    self.confFile = '/dev/null'
  def set(self):
    # parse conf file
    config = ConfigParser.RawConfigParser()
    config.read(self.confFile)
    configuredActions = config.sections()
    for action in configuredActions:
      key = config.get(action, 'key')
      trigger = config.get(action, 'trigger')
      hold = config.get(action, 'hold')
      cmd = config.get(action, 'cmd')
      self.conf.append({"key":int(key), "trigger":trigger, "hold":int(hold), "cmd":cmd, "thread":None})

def main(argv):
  inputfile = ''
  outputfile = ''
  #
  # process command line arguments
  #
  try:
    opts, args = getopt.getopt(argv,"h:c:",["config="])
  except getopt.GetoptError:
    print 'mpr121d -c <configfile>'
    sys.exit(2)
  if not opts:
      inputfile = '/etc/mpr121d.conf'
  else:
    for opt, arg in opts:
      if opt == '-h':
        print 'mpr121d -c <configfile>'
        sys.exit()
      elif opt in ("-c", "--config"):
        inputfile = arg
  if os.path.isfile(inputfile):
    print 'Config file', inputfile
  else:
    print 'Invalid config file',inputfile
    sys.exit()
  #
  # process conf file
  #
  keyboard = keyboardActions()
  keyboard.confFile = inputfile 
  keyboard.set();
  #
  # start mpr121 interface
  #
  mprCap = MPR121.MPR121()
  if not mprCap.begin():
    print('Error initializing MPR121.  Check your wiring!')
    sys.exit()
  #
  # loop for mpr event
  #
  lastTouched = mprCap.touched()
  time.sleep(0.1)
  while True:
    currentTouched = mprCap.touched()
    #
    # check keyboard event for configured keys
    #
    for action in keyboard.conf:
      pin_bit = 1 << action["key"]
      if currentTouched & pin_bit and not lastTouched & pin_bit:
        if action["trigger"] == "press":
          action["thread"] = keyaction(action["key"], action["trigger"], action["hold"], action["cmd"])
          action["thread"].start()
      if not currentTouched & pin_bit and lastTouched & pin_bit:
        if action["trigger"] == "release":
          action["thread"] = keyaction(action["key"], action["trigger"], action["hold"], action["cmd"])
          action["thread"].start()
        else:
          action["thread"].stop()
    lastTouched = currentTouched
    time.sleep(0.1)

if __name__ == "__main__":
  main(sys.argv[1:])
