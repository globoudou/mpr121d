# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Usable with Raspberry pi and sensitive mpr121 keyboard
* This is a simple mpr121 systemd script to launch system commands or scripts
* 3 keyboard events : on press, on release and on min time pressed

### How do I get set up? ###

```
#!bash
git clone https://bitbucket.org/globoudou/mpr121d/
cd mpr121d
./install.sh
sudo vi /etc/mpr121d.conf
sudo systemctl restart mpr121d
```

### Files ###
```
├── etc
│   ├── mpr121d.conf
│   └── systemd
│       └── system
│           └── mpr121d.service
└── usr
    └── local
        └── sbin
            └── mpr121d.py

```